from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView
from apps.users import views

urlpatterns = [
    path('create/', views.UserCreateAccountAPIView.as_view(), name='create_new_user'),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/verify/', TokenVerifyView.as_view(), name='token_verify'),
    path('refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('me/', views.AccountUserAPIView.as_view(), name='about_user'),
]
