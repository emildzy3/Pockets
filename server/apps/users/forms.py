from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserChangeForm, UserCreationForm

User = get_user_model()


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = '__all__'


class CustomReaderCreationForm(UserCreationForm):
    class Meta():
        model = User
        fields = ('email', 'username', 'password1', 'password2')
