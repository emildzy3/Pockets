from django.contrib.auth import get_user_model
from django.shortcuts import render
from rest_framework import generics
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated

from apps.users.serializers import UserCreateAccountSerializer, AccountUserSerializer

User = get_user_model()


class AccountUserAPIView(generics.RetrieveAPIView):
    """
    Getting information about an authorized user
    """
    serializer_class = AccountUserSerializer
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()

    def get_object(self):
        obj = get_object_or_404(User, id=self.request.user.id)
        return obj


class UserCreateAccountAPIView(generics.CreateAPIView):
    """
    Service for creating a new user
    """
    queryset = User.objects.all()
    serializer_class = UserCreateAccountSerializer

