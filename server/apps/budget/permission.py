from rest_framework.permissions import BasePermission


class IsOwnerObject(BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.owner == request.user
