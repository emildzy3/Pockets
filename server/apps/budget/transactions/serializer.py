from django.contrib.auth import get_user_model
from rest_framework import serializers

from apps.budget.transactions.models import Category, Transactions

User = get_user_model()


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'type', 'name')


class CategoryWithTransactionSerializer(serializers.ModelSerializer):
    category_amount = serializers.DecimalField(max_digits=10, decimal_places=2, read_only=True)

    class Meta:
        model = Category
        fields = ('name', 'category_amount')


class TransactionListSerializer(serializers.ModelSerializer):
    name_category_transaction = serializers.CharField(read_only=True)
    type_operation = serializers.CharField(read_only=True)

    class Meta:
        model = Transactions
        fields = ('type_operation', 'name_category_transaction', 'category', 'amount', 'date_operation')
