from django.db.models import Sum, DecimalField, Q, F
from django.db.models.functions import Coalesce
from rest_framework import viewsets
from apps.budget.transactions.filter import CategoryPeriodFilter
from apps.budget.transactions.models import Transactions
from apps.budget.transactions.serializer import CategoryWithTransactionSerializer


class BaseTransactionsApiView(viewsets.ModelViewSet):
    """
    Gets the user from the request
    """

    def perform_create(self, serializer):
        serializer.validated_data['owner'] = self.request.user
        serializer.save()


def get_category_with_sum_operation(queryset, data):
    """
    A service that filters costs and incomes by categories depending on the period selected by the user
    """
    list_category_with_sum_transaction = \
        CategoryPeriodFilter(queryset=queryset,
                             data=data).get_list_category_with_transactions_filtered_date

    serializer = CategoryWithTransactionSerializer(list_category_with_sum_transaction, many=True).data
    return serializer


def calculate_amount_transactions_for_period(date_query_filtered):
    """
    Service that calculates the total expenses for the selected period
    """
    sum_all_transactions = date_query_filtered.aggregate(
        sum_income=Coalesce(Sum('amount', filter=Q(category__type=1)), 0, output_field=DecimalField()),
        sum_expenses=Coalesce(Sum('amount', filter=Q(category__type=0)), 0, output_field=DecimalField()))
    return sum_all_transactions


def get_transaction_queryset(owner):
    """
    Service giving a general list of transactions
    """
    queryset = Transactions.objects.filter(owner=owner).annotate(
        name_category_transaction=F('category__name'),
        type_operation=F('category__type')
    ).order_by('-date_operation')
    return queryset



