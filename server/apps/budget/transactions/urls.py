from django.urls import include, path
from rest_framework.routers import SimpleRouter

from apps.budget.transactions.views import CategoryAPIView, TransactionsAPIView

router = SimpleRouter()
router.register(r'category', CategoryAPIView, basename='category')
router.register(r'transactions', TransactionsAPIView, basename='transactions')

urlpatterns = [
    path('', include(router.urls)),

]
