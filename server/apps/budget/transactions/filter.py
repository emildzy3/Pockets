from datetime import datetime, date
from django.db.models import Sum, Q
from django_filters import rest_framework as filters, DateFilter


# http://127.0.0.1:8000/api/accounting/transactions/?start_date=2022-05-03&end_date=2022-05-10
class TransactionPeriodFilter(filters.FilterSet):
    start_date = DateFilter(field_name='date_operation', lookup_expr='gte')
    end_date = DateFilter(field_name='date_operation', lookup_expr='lte')


class CategoryPeriodFilter(filters.FilterSet):
    start_date = DateFilter(field_name='transactions__date_operation', lookup_expr='gte')
    end_date = DateFilter(field_name='transactions__date_operation', lookup_expr='lte')

    @property
    def get_list_category_with_transactions_filtered_date(self):
        parent = super().qs

        start_date = self.data.get('start_date', date(2021, 12, 30))
        end_date = self.data.get('end_date', datetime.now())

        list_category_with_transaction_filtered_date = parent.annotate(
            category_amount=Sum('transactions__amount',
                                filter=(
                                    Q(transactions__date_operation__gte=start_date,
                                      transactions__date_operation__lte=end_date)
                                )
                                )
        ).select_related('owner').order_by('-category_amount')

        return list_category_with_transaction_filtered_date
