from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class Category(models.Model):
    TYPE_CHOICES = (
        (0, 'Расход'),
        (1, 'Доход'),
    )

    type = models.PositiveSmallIntegerField(choices=TYPE_CHOICES)
    name = models.CharField(verbose_name='Название категории', max_length=50)
    owner = models.ForeignKey(User, verbose_name='Владелец', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.pk}>>>{self.type} - {self.name} - {self.owner}'


class Transactions(models.Model):
    owner = models.ForeignKey(User, verbose_name='Владелец', on_delete=models.CASCADE)
    category = models.ForeignKey('Category', verbose_name='Категории', on_delete=models.CASCADE)
    amount = models.DecimalField(verbose_name='Сумма транзакции', max_digits=10, decimal_places=2)
    date_operation = models.DateField(verbose_name='Дата транзакции')

    def __str__(self):
        return f'{self.owner} - {self.category.name} - {self.amount}'
