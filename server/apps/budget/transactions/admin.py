from django.contrib import admin
from apps.budget.transactions.models import Category, Transactions

admin.site.register(Category)
admin.site.register(Transactions)
