from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from apps.budget.permission import IsOwnerObject
from apps.budget.transactions.filter import TransactionPeriodFilter, CategoryPeriodFilter
from apps.budget.transactions.models import Category
from apps.budget.transactions.pagination import TransactionsPagination, CategoryPagination
from apps.budget.transactions.serializer import CategorySerializer, TransactionListSerializer, \
    CategoryWithTransactionSerializer
from django_filters import rest_framework as filters
from apps.budget.transactions.services import BaseTransactionsApiView, get_category_with_sum_operation, \
    calculate_amount_transactions_for_period, get_transaction_queryset


class CategoryAPIView(BaseTransactionsApiView):
    """
    Getting and changing user categories
    """
    serializer_class = CategorySerializer
    pagination_class = CategoryPagination
    filterset_class = CategoryPeriodFilter
    permission_classes = (IsAuthenticated, IsOwnerObject)

    def get_queryset(self):
        queryset = Category.objects.filter(owner=self.request.user)
        return queryset

    @action(methods=['get'], detail=False)
    def list_category_with_sum_operations(self, request):
        """
        Service for combining categories added by the user and the amount of transactions for these categories for
        a certain period of time
        """
        list_category_with_sum_transaction = get_category_with_sum_operation(self.get_queryset(),
                                                                             request.query_params)
        page = self.paginate_queryset(list_category_with_sum_transaction)
        if page is not None:
            serializer = CategoryWithTransactionSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        return Response(list_category_with_sum_transaction)


class TransactionsAPIView(BaseTransactionsApiView):
    """
    A service that allows you to add, edit, change and delete user expenses and income
    """
    serializer_class = TransactionListSerializer
    pagination_class = TransactionsPagination
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = TransactionPeriodFilter
    permission_classes = (IsAuthenticated, IsOwnerObject)

    def get_queryset(self):
        return get_transaction_queryset(owner=self.request.user)

    @action(methods=['get'], detail=False)
    def get_transaction_amount(self, request):
        """
        Service that returns total income and expenses for all categories for the period
        """
        date_query_filtered = self.filter_queryset(self.get_queryset())
        sum_all_transactions = calculate_amount_transactions_for_period(date_query_filtered)
        return Response(sum_all_transactions)
