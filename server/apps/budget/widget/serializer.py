from rest_framework import serializers
from apps.budget.widget.models import Widget


class WidgetListSerializer(serializers.ModelSerializer):
    current_amount = serializers.DecimalField(max_digits=10, decimal_places=2, read_only=True)
    date_creation = serializers.CharField(read_only=True)
    date_end = serializers.CharField(read_only=True)

    class Meta:
        model = Widget
        fields = ('id', 'category', 'limit', 'expiration_date', 'criterion', 'colour', 'date_creation',
                  'date_end', 'current_amount')
