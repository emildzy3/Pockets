from django.apps import AppConfig


class WidgetConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.budget.widget'

    def ready(self):
        import apps.budget.widget.signals
