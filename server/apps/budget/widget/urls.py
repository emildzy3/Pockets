from django.urls import include, path
from rest_framework.routers import SimpleRouter
from apps.budget.widget.views import WidgetListAPIView

router = SimpleRouter()
router.register(r'', WidgetListAPIView, basename='widget')

urlpatterns = [
    path('', include(router.urls)),
]
