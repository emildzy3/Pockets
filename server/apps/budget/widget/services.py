from django.db.models import Sum, Q, DecimalField, F
from django.db.models.functions import Coalesce
from apps.budget.widget.models import Widget


def get_queryset_for_widget(owner):
    """
    Service returning a queryset for a user widget sorted for a certain period
    """
    queryset = Widget.objects.filter(owner=owner).annotate(
            current_amount=Coalesce(Sum('category__transactions__amount',
                                        filter=(
                                                Q(category__transactions__date_operation__gte=F('date_creation')) &
                                                Q(category__transactions__date_operation__lte=F('date_end'))
                                        )
                                        ), 0, output_field=DecimalField())). \
            select_related('owner', 'category').order_by('-id')
    return queryset