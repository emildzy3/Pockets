from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from apps.budget.permission import IsOwnerObject
from apps.budget.widget.serializer import WidgetListSerializer
from apps.budget.widget.services import get_queryset_for_widget


class WidgetListAPIView(viewsets.ModelViewSet):
    """
    Getting and changing user widget
    """
    serializer_class = WidgetListSerializer
    permission_classes = (IsAuthenticated, IsOwnerObject)

    def perform_create(self, serializer):
        serializer.validated_data['owner'] = self.request.user
        serializer.save()

    def get_queryset(self):
        return get_queryset_for_widget(self.request.user)
