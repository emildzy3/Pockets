from django.contrib.auth import get_user_model
from django.db import models
from colorfield.fields import ColorField
from apps.budget.transactions.models import Category

User = get_user_model()


class Widget(models.Model):
    CRITERION_CHOICES = (
        (0, 'Меньше'),
        (1, 'Больше'),
    )

    EXPIRATION_DATE_CHOICES = (
        (1, 'День'),
        (7, 'Неделя'),
        (30, 'Месяц')
    )

    owner = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Владелец')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Категория')
    limit = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Лимит')
    expiration_date = models.PositiveSmallIntegerField(choices=EXPIRATION_DATE_CHOICES, verbose_name='Срок действия')
    criterion = models.PositiveSmallIntegerField(choices=CRITERION_CHOICES, verbose_name='Критерий')
    colour = ColorField(format="hex", default='#E5F5FF', verbose_name='Цвет')
    date_creation = models.DateField(auto_now_add=True, verbose_name='Дата создания')
    date_end = models.DateField(verbose_name='Дата окончания', blank=True, default='2020-02-02')

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return f'{self.pk} >>>{self.owner} - {self.limit} - {self.expiration_date} дней '
