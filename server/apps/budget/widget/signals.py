from django.dispatch import receiver
from django.db.models.signals import post_save
from apps.budget.widget.models import Widget
from dateutil.relativedelta import relativedelta


@receiver(post_save, sender=Widget)
def widget_end_date_calculation(instance, created, **kwargs):
    """
    Signal that calculates the value of the current amount spent
    """
    if created:
        calculated_date_value = instance.date_creation + relativedelta(days=instance.expiration_date)
        instance.date_end = calculated_date_value
        instance.save()


