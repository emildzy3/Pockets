import datetime
import json
from django.contrib.auth import get_user_model
from django.db.models import F
from rest_framework import status
from rest_framework.test import APITestCase, force_authenticate, APIClient
from django.urls import reverse
from rest_framework_simplejwt.tokens import RefreshToken

from apps.budget.transactions.models import Category, Transactions
from apps.budget.transactions.serializer import CategorySerializer, TransactionListSerializer
from apps.budget.transactions.views import CategoryAPIView
from apps.budget.widget.models import Widget
from apps.budget.widget.serializer import WidgetListSerializer

User = get_user_model()


class BudgetApiTestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(email='test@ya.ru', username='test', password='111')
        self.user_2 = User.objects.create_user(email='test2@ya.ru', username='test2', password='111')
        self.user_3 = User.objects.create_user(email='test3@ya.ru', username='test3', password='111')

        self.refresh_user = RefreshToken.for_user(self.user)
        self.refresh_user_2 = RefreshToken.for_user(self.user_2)
        self.refresh_user_3 = RefreshToken.for_user(self.user_3)

        self.headers_user = {"HTTP_AUTHORIZATION": f'Bearer {self.refresh_user.access_token}'}
        self.headers_user_2 = {"HTTP_AUTHORIZATION": f'Bearer {self.refresh_user_2.access_token}'}
        self.headers_user_3 = {"HTTP_AUTHORIZATION": f'Bearer {self.refresh_user_3.access_token}'}
        self.fail_headers = {"HTTP_AUTHORIZATION": f'HTTP {self.refresh_user_3.access_token}'}

        self.category_user_1_1 = Category.objects.create(type=1, name='Зарплата', owner=self.user)
        self.category_user_1_2 = Category.objects.create(type=0, name='Супермаркет', owner=self.user)
        self.category_user_1_3 = Category.objects.create(type=1, name='Пенсия', owner=self.user)

        self.category_user_2_1 = Category.objects.create(type=0, name='Супермаркеты_2', owner=self.user_2)
        self.category_user_2_1 = Category.objects.create(type=0, name='Пенсия_2', owner=self.user_2)

        self.transaction_user_1_1 = Transactions.objects.create(owner=self.user, category=self.category_user_1_1,
                                                                amount=10000.000,
                                                                date_operation=datetime.date.today())

        self.widget_user_2_1 = Widget.objects.create(owner=self.user_2, category=self.category_user_2_1, limit="100.00",
                                                     expiration_date=7, criterion=1)


class CategoryApiTestCase(BudgetApiTestCase):
    def test_delete_category_not_owner(self):
        total_number_of_category = Category.objects.all().count()

        url = reverse('category-detail', args=(self.category_user_1_1.pk,))
        response = self.client.delete(url, **self.headers_user_3)

        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)
        self.assertEqual(Category.objects.all().count(), total_number_of_category)

    def test_delete_category_owner(self):
        total_number_of_category = Category.objects.all().count()

        url = reverse('category-detail', args=(self.category_user_1_1.pk,))
        response = self.client.delete(url, **self.headers_user)

        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        self.assertEqual(Category.objects.all().count(), total_number_of_category - 1)

    def test_get_category_list(self):
        url = reverse('category-list')
        response = self.client.get(url, **self.headers_user)
        list_category_from_extradition = Category.objects.filter(owner=self.user.pk)
        serializer_local = CategorySerializer(list_category_from_extradition, many=True).data

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(serializer_local, response.data['results'])

    def test_get_category_list_by_unauthorized_user(self):
        url = reverse('category-list')
        response = self.client.get(url)
        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)

    def test_post(self):
        url = reverse('category-list')
        data = {
            'type': 0,
            "name": 'Премия'
        }
        json_data = json.dumps(data)
        response = self.client.post(url, **self.headers_user, data=json_data, content_type="application/json")

        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(self.user, Category.objects.get(name='Премия').owner)
        self.assertEqual(6, Category.objects.all().count())

    def test_post_by_unauthorized_user(self):
        url = reverse('category-list')
        data = {
            'type': 0,
            "name": 'Премия'
        }
        json_data = json.dumps(data)
        response = self.client.post(url, **self.fail_headers, data=json_data, content_type="application/json")

        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)
        self.assertEqual(5, Category.objects.all().count())


class WidgetApiTestCase(BudgetApiTestCase):
    def test_delete_widget_not_owner(self):
        total_number_of_widget = Widget.objects.all().count()

        url = reverse('widget-detail', args=(self.widget_user_2_1.pk,))
        response = self.client.delete(url, **self.headers_user)

        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)
        self.assertEqual(Widget.objects.all().count(), total_number_of_widget)

    def test_delete_widget_owner(self):
        total_number_of_widget = Widget.objects.all().count()

        url = reverse('widget-detail', args=(self.widget_user_2_1.pk,))
        response = self.client.delete(url, **self.headers_user_2)

        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        self.assertEqual(Widget.objects.all().count(), total_number_of_widget - 1)

    def test_get_widget_list(self):
        url = reverse('widget-list')
        response = self.client.get(url, **self.headers_user)
        list_widget_from_extradition = Widget.objects.filter(owner=self.user.pk)
        serializer_local = WidgetListSerializer(list_widget_from_extradition, many=True).data

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(serializer_local, response.data)

    def test_get_widget_list_by_unauthorized_user(self):
        url = reverse('widget-list')
        response = self.client.get(url)
        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)

    def test_post(self):
        url = reverse('widget-list')
        data = {
            "category": self.category_user_1_1.pk,
            "limit": "100.00",
            "expiration_date": 7,
            "criterion": 1
        }
        json_data = json.dumps(data)
        response = self.client.post(url, **self.headers_user, data=json_data, content_type="application/json")

        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(self.user, Widget.objects.get(category=self.category_user_1_1.pk).owner)
        self.assertEqual(2, Widget.objects.all().count())

    def test_post_by_unauthorized_user(self):
        url = reverse('widget-list')
        data = {
            "category": self.category_user_1_1.pk,
            "limit": "100.00",
            "expiration_date": 7,
            "criterion": 1
        }
        json_data = json.dumps(data)
        response = self.client.post(url, **self.fail_headers,  data=json_data, content_type="application/json")

        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)
        self.assertEqual(1, Widget.objects.all().count())


class TransactionsApiTestCase(BudgetApiTestCase):
    def test_update_transaction_not_owner(self):
        url = reverse('transactions-detail', args=(self.transaction_user_1_1.pk,))
        data = {
            "amount": 333
        }
        json_data = json.dumps(data)

        response = self.client.patch(url, **self.headers_user_2, data=json_data, content_type="application/json")
        self.transaction_user_1_1.refresh_from_db()

        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)
        self.assertEqual('10000.00', str(self.transaction_user_1_1.amount))

    def test_update_transaction_owner(self):
        url = reverse('transactions-detail', args=(self.transaction_user_1_1.pk,))
        data = {
            "amount": 333
        }
        json_data = json.dumps(data)

        response = self.client.patch(url, **self.headers_user, data=json_data, content_type="application/json")
        self.transaction_user_1_1.refresh_from_db()

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(333, self.transaction_user_1_1.amount)

    def test_delete_transaction_not_owner(self):
        total_number_of_transactions = Transactions.objects.all().count()

        url = reverse('transactions-detail', args=(self.transaction_user_1_1.pk,))
        response = self.client.delete(url, **self.headers_user_2)

        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)
        self.assertEqual(Transactions.objects.all().count(), total_number_of_transactions)

    def test_delete_transaction_owner(self):
        total_number_of_transactions = Transactions.objects.all().count()

        url = reverse('transactions-detail', args=(self.transaction_user_1_1.pk,))
        response = self.client.delete(url, **self.headers_user)

        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        self.assertEqual(Transactions.objects.all().count(), total_number_of_transactions - 1)

    def test_get_transactions_list(self):
        url = reverse('transactions-list')
        response = self.client.get(url, **self.headers_user)

        list_transactions_from_extradition = Transactions.objects.filter(owner=self.user.pk).annotate(
            name_category_transaction=F('category__name'),
            type_operation=F('category__type')
        ).order_by('-date_operation')
        serializer_local = TransactionListSerializer(list_transactions_from_extradition, many=True).data

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(serializer_local, response.data['results'])

    def test_get_transactions_list_by_unauthorized_user(self):
        url = reverse('transactions-list')
        response = self.client.get(url)
        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)

    def test_post(self):
        url = reverse('transactions-list')
        data = {
            "category": self.category_user_2_1.pk,
            "amount": 140.00,
            "date_operation": "2022-05-15"
        }
        json_data = json.dumps(data)
        response = self.client.post(url, **self.headers_user, data=json_data, content_type="application/json")
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(2, Transactions.objects.all().count())

    def test_post_by_unauthorized_user(self):
        url = reverse('transactions-list')
        data = {
            "category": self.category_user_2_1.pk,
            "amount": 140.00,
            "date_operation": "2022-05-15"
        }

        json_data = json.dumps(data)
        response = self.client.post(url, **self.fail_headers, data=json_data, content_type="application/json")

        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)
        self.assertEqual(1, Widget.objects.all().count())