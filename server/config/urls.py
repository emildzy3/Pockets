from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from apps.swagger.settings import urlpatterns as doc_url

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/users/', include('apps.users.urls')),
    path('api/accounting/', include('apps.budget.transactions.urls')),
    path('api/widget/', include('apps.budget.widget.urls'))

]
urlpatterns += doc_url

if settings.DEBUG:
    import debug_toolbar
    from django.conf.urls.static import static

    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]
