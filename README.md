# Pockets

## О Pockets
___
Pockets - ресурс для учета и планирования расходов. [Техническое задание/описание проекта](https://github.com/emildzy3/practice2021-django-stub/blob/main/server/apps/doc/%D0%9F%D1%80%D0%BE%D0%B5%D0%BA%D1%82%20Python%6021.docx.pdf) предоставлено компанией [SibDev](https://github.com/SibdevPro) в рамках проведения летней практики в 2021 году.



## Запуск и установка проекта 
___
Инструкция по запуску и установке проекта представлена [здесь](https://github.com/emildzy3/practice2021-django-stub/blob/main/server/apps/doc/start_project.md).
